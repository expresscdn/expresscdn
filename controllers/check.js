function isUserLogged(req){
	if(req.user){
		return true;
	} else {
		return false;
	}
}

function isModLogged(req){
	if(isUserLogged(req)){
		return req.user.isMod;
	} else {
		return false;
	}
}

function isAdminLogged(req){
	if(isUserLogged(req)){
		return req.user.isAdmin;
	}
	else {
		return false;
	}
}

module.exports.isUserLogged = isUserLogged;
module.exports.isModLogged = isModLogged;
module.exports.isAdminLogged = isAdminLogged;