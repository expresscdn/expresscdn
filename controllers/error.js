function send(code, name){
	var err = new Error(name);
	err.status = code;
	return err;
}

module.exports.send = send;