//Dependencies
var express = require('express');
var ect = require('ect');
var i18n = require('i18n');
var path = require('path');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var session = require('express-session');
var mongoStore = require('connect-mongo')(session);
var passport = require('passport');
var localStrategy = require('passport-local').Strategy;
var multer = require('multer');

//DB models
var User = require('./models/User');

//Config
var config = require('./config.json');

//Route controllers
var index = require('./routes/index');
var login = require('./routes/login');
var logout = require('./routes/logout');
var register = require('./routes/register');
var setLang = require('./routes/setLang');
var admin = require('./routes/admin');
var profile = require('./routes/profile');
var upload = require('./routes/upload');
var video = require('./routes/video');

var app = express();

//View engine setup
var ectRenderer = ect({ watch: true, root: __dirname + '/views', ext: '.ect' });
app.set('view engine', 'ect');
app.engine('ect', ectRenderer.render);

//Locales
i18n.configure({
    locales: config.locale.catelog || ['en', 'es'],
    defaultLocale: config.locale.default || "en",
    cookie: config.locale.cookie || "expresscdn.i18n",
    prefix: config.locale.prefix || "expresscdn-i18n-",
    directory: config.locale.directory || "./i18n"
});

//Connect to mongoDB
mongoose.connect(config.database.route, config.database.options);

//App Setup
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(session({
    name: config.session.name,
    secret: config.session.secret,
    resave: false,
    saveUninitialized: true,
    store: new mongoStore({
        mongooseConnection: mongoose.connection
    })
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(require('less-middleware')(config.routes.less || "./public"));
app.use(express.static(config.routes.static || "./public"));
app.use(multer({dest:'./public/media/'}));


app.use(i18n.init);

app.locals = {
    name: config.app.name || "ExpressCDN",
    favicon: config.custom.favicon || "/icon/favicon.ico",
    appleicon: config.custom.appleicon || "/icon/appleicon.png",
    prefix: config.routes.prefix || "ec-",
    catalog: config.locale.catalog || ["en", "es"],
    separator: config.custom.separator || " | "
};

//PassportJS
passport.use(User.createStrategy());
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

//Routes
app.use('/', index);
app.use('/'+app.locals.prefix+'login', login);
app.use('/'+app.locals.prefix+'logout', logout);
app.use('/'+app.locals.prefix+'register', register);
app.use('/'+app.locals.prefix+'setlang', setLang);
app.use('/'+app.locals.prefix+'admin', admin);
app.use('/'+app.locals.prefix+'profile', profile);
app.use('/'+app.locals.prefix+'upload', upload);
app.use('/'+app.locals.prefix+'video', video);

//Catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

//Error handlers
//Development
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('page_error', {
            title: err.message,
            message: err.message,
            status: err.status,
            error: err,
            req: req
        });
    });
}

//Production
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('page_error', {
        title: err.message,
        message: err.message,
        status: err.status,
        error: {},
        req: req
    });
});


module.exports = app;
