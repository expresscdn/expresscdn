var express = require('express');
var passport = require('passport');
var router = express.Router();
var config = require('../config.json');

var User = require('../models/User');
var MediaContent = require('../models/MediaContent');

/* GET home page.
 * Use case: Watch media.
 */
router.get('/', function(req, res) {	
	MediaContent.find().sort({date: -1}).limit(12).exec( 
		function(err, mediaContent) {
			res.render('page_index', { req: req, mediaContent: mediaContent });
			console.log(mediaContent);
		}
	);
});

module.exports = router;
