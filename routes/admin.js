var express = require('express');
var router = express.Router();
var async = require('async');
var config = require('../config.json');

var check = require('../controllers/check');
var error = require('../controllers/error');

var User = require('../models/User');
var Group = require('../models/Group');
var MediaCategory = require('../models/MediaCategory');
var MediaContent = require('../models/MediaContent');

/* GET home page.
 * Use case: Watch media.
 */
router.get('/', function(req, res, next) {
	if(!check.isUserLogged(req)){
		next(error.send(403, "Forbidden"));
	} else {
		async.parallel({
			countUsers: function(callback){
				User.count({}, function(err, count){
					if(err){
						callback(err, null);
					} else {
						callback(null, count);
					}
				});
			},
			countGroups: function(callback){
				Group.count({}, function(err, count){
					if(err){
						callback(err, null);
					} else {
						callback(null, count);
					}
				});
			},
			countCategories: function(callback){
				MediaCategory.count({}, function(err, count){
					if(err){
						callback(err, null);
					} else {
						callback(null, count);
					}
				});
			},
			countMedia: function(callback){
				MediaContent.count({}, function(err, count){
					if(err){
						callback(err, null);
					} else {
						callback(null, count);
					}
				});
			}
		},
		function(err, results){
			req.countUsers = results.countUsers;
			req.countGroups = results.countGroups;
			req.countCategories = results.countCategories;
			req.countMedia = results.countMedia;
			res.render('page_admin', { title: res.__("Admin"), req: req });
		});
	}
});

router.get('/users', function(req, res, next){
	if(!check.isUserLogged(req)){
		next(error.send(403, "Forbidden"));
	} else {
		res.render('page_admin_users', {title: res.__("Users - Admin"), req: req});
	}
});

module.exports = router;
