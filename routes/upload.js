var express = require('express');
var router = express.Router();
var passport = require('passport');
var config = require('../config.json');
var fs = require('fs');
var srt2vtt = require('srt2vtt');
var os = require('os');
var exec = require('child_process').exec;

var check = require('../controllers/check');
var error = require('../controllers/error');
var logger = require('../controllers/logger');

var MediaContent = require('../models/MediaContent');

router.get('/', function(req, res, next) {
	if(!check.isUserLogged(req)){
		next(error.send(403, "Forbidden"));
	} else {
		res.render('page_upload', { title: res.__("Upload"), req: req });
	}
});

router.post('/', function(req, res) {
	if(!check.isUserLogged(req)){
		next(error.send(403, "Forbidden"));
	} else {
		var dateNow = Date.now();
		fs.mkdirSync('public/media/' + dateNow);
		fs.renameSync(req.files.inputFile.path, 'public/media/' + dateNow + '/OriginalVideo.' + req.files.inputFile.extension);
		if (req.files.inputSub)
		{
			fs.rename(req.files.inputSub.path, 'public/media/' + dateNow + '/Subtitles.' + req.files.inputSub.extension, function (err) {
				if (err) throw err;
				console.log('renamed complete');
				if (req.files.inputSub.extension == 'srt')
				{
					var srtData = fs.readFileSync('public/media/' + dateNow + '/Subtitles.srt');
					srt2vtt(srtData, function(err, vttData) {
						if (err) throw err;
						fs.writeFileSync('public/media/' + dateNow + '/Subtitles.vtt', vttData);
					});
					console.log('vtt complete');
				}
			});
		}
		
		var ffmpegPath, sourceVidPath, targetVidH264Path, targetVidWebMPath, targetSSPath;
		if(/^win/.test(os.platform())){
			console.log(os.platform());
			ffmpegPath = 'public\\utils\\ffmpeg';
			sourceVidPath = 'public\\media\\' + dateNow + '\\OriginalVideo.' + req.files.inputFile.extension;
			targetSSPath = 'public\\media\\' + dateNow + '\\img%03d.jpg';
			targetVidH264Path = 'public\\media\\' + dateNow + '\\Video.mp4';
			targetVidWebMPath = 'public\\media\\' + dateNow + '\\Video.webm';
		} else {
			console.log(os.platform());
			ffmpegPath = 'public/utils/ffmpeg';
			sourceVidPath = 'public/media/' + dateNow + '/OriginalVideo.' + req.files.inputFile.extension;
			targetSSPath = 'public/media/' + dateNow + '/img%03d.jpg';
			targetVidH264Path = 'public/media/' + dateNow + '/Video.mp4';
			targetVidWebMPath = 'public/media/' + dateNow + '/Video.webm';
		}

		exec(ffmpegPath + ' -ss 3 -i "' + sourceVidPath + '" -vf "select=gt(scene\\,0.5)" -frames:v 5 -vsync vfr -s 1280x720 "' + targetSSPath + '"', function(error, stdout, stderr) {
			logger.info('Starting screenshoot capture ('+ dateNow +')...');
			logger.info('FFMpeg STDOUT: ' + stdout);
			logger.error('FFMpeg STDERR: ' + stderr);
			if (error !== null) {
				logger.error('Exec ('+ dateNow +') error: ' + error);
			}
			logger.info('Screenshot capture finished ('+ dateNow +').');
		});
		
		exec(ffmpegPath + ' -i "' + sourceVidPath + '" -c:v libx264 -preset ultrafast -c:a libvo_aacenc -ac 2 "' + targetVidH264Path + '"', function(error, stdout, stderr) {
			logger.info('Starting H264 codification ('+ dateNow +')...');
			logger.info('FFMpeg STDOUT: ' + stdout);
			logger.error('FFMpeg STDERR: ' + stderr);
			if (error !== null) {
				logger.error('Exec ('+ dateNow +') error: ' + error);
			}
			logger.info('H264 codification finished ('+ dateNow +').');
		});

		exec(ffmpegPath + ' -i "' + sourceVidPath + '" -c:v libvpx -b:v 1M -c:a libvorbis "' + targetVidWebMPath + '"', function(error, stdout, stderr) {
			logger.info('Starting WebM codification ('+ dateNow +')...');
			logger.info('FFMpeg STDOUT: ' + stdout);
			logger.error('FFMpeg STDERR: ' + stderr);
			if (error !== null) {
				logger.error('Exec ('+ dateNow +') error: ' + error);
			}
			logger.info('WebM codification finished ('+ dateNow +').');
		});
		
		var mediaContent = new MediaContent();
		
		if (req.body.videoTitle)
			mediaContent.name = req.body.videoTitle;
		else
			mediaContent.name = dateNow;
				
		mediaContent.description = req.body.descriptionArea;		
		mediaContent.user = req.user;
		mediaContent.date = dateNow;
		mediaContent.mediaFile = dateNow;
		
		mediaContent.save(function (err, product, numberAffected) {
			if (err) throw err;
		});
		res.send("ok");
	}
});

module.exports = router;
