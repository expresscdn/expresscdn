var express = require('express');
var router = express.Router();
var passport = require('passport');
var config = require('../config.json');

/* GET login page.
 * Use case: User login.
 */
router.get('/', function(req, res) {
  res.render('page_login', { title: res.__("Login"), req: req });
});

router.post('/', passport.authenticate('local', { successRedirect: '/',
                                 failureRedirect: '/ec-login'})
);

module.exports = router;
