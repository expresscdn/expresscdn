var express = require('express');
var router = express.Router();
var passport = require('passport');
var config = require('../config.json');

var User = require('../models/User');

/* GET home page.
 * Use case: Watch media.
 */
router.get('/', function(req, res) {		
	res.render('page_register', { title: res.__("Register"), req: req });
});

router.post('/', function(req, res) {
	var user = new User({ username: req.body.username, email: req.body.email });
	
	User.register( user, req.body.password, function(err, User) {
        if (err) {
          return res.render("page_register", { title: res.__("Register"), req: req, info: "Sorry. That username already exists. Try again."});
        }
		
		user.email = req.body.email;
		user.save(function (err, product, numberAffected) {
			if (err) {
				return res.render("page_register", { title: res.__("Register"), req: req, info: "Sorry!"});
			}
		});
		
        passport.authenticate('local')(req, res, function () {
          res.redirect('/');
        });
    });
});

module.exports = router;
