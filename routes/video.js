var express = require('express');
var router = express.Router();
var passport = require('passport');
var config = require('../config.json');

var check = require('../controllers/check');
var error = require('../controllers/error');
var logger = require('../controllers/logger');

var MediaContent = require('../models/MediaContent');

router.get('/', function(req, res, next) {
	next(error.send(403, "Forbidden"));
});

router.get('/:videoID', function(req, res, next) {
	
	var mediaContent = MediaContent.findOne({mediaFile : req.params.videoID}).exec(
		function(err, mediaContent) {
			res.render('page_video', { req: req, mediaContent: mediaContent });
		}
	);
});

module.exports = router;