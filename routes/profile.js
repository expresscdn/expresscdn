var express = require('express');
var passport = require('passport');
var router = express.Router();
var config = require('../config.json');

var check = require('../controllers/check');
var error = require('../controllers/error');

var User = require('../models/User');

/* GET home page.
 * Use case: Watch media.
 */

router.param('username', function(req, res, next, username){
	User.findOne({username: req.params.username})
		.populate('mediaFiles')
		.populate('group')
		.exec(function(err, account){
			if(account){
				req.account = account;
				req.countViews = 0;
				for(var i=0; i<account.mediaFiles.length; i++){
					req.countViews += account.mediaFiles[i].totalViews;
				}
				next();
			} else {
				next(error.send(404, "Not found"));
			}
	});
});
router.get('/:username', function(req, res, next) {
	res.render('page_profile', {title: req.account.username, req: req});
});

router.get('/:username/media', function(req, res, next){
	res.render('page_profile_media', {title: res.__("Media of ")+req.account.username, req: req});
});

router.get('/:username/edit', function(req, res, next){
	if(req.user._id !== req.account._id && !check.isAdminLogged(req)){
		next(error.send(403, "Forbidden"));
	} else {
		res.render('page_profile_edit', {title: res.__("Edit user profile of ")+req.account.username, req: req});
	}
});

// POST EDIT

router.get('/:username/delete', function(req, res, next){
	if(req.user._id !== req.account._id && !check.isAdminLogged(req)){
		next(error.send(403, "Forbidden"));
	} else {
		res.render('page_profile_delete', {title: res.__("Delete user ")+req.account.username, req: req});
	}
});

// POST DELETE

module.exports = router;