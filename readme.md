#Express CDN

Proyecto de CDN basado en Express para la asignatura de Tecnología 
Multimedia de la Escuela Técnica Superior de Ingeniería de Sistemas Informáticos,
perteneciente a la Universidad Politécnica de Madrid. (ETSISI-UPM)

##Contributors

* Ángel González
* Alan Mark Sousa
* Dulce Lariz
* Juan Robisco
* Borja Guzmán

##Powered by
###Frontend

* JQuery (MIT) http://jquery.com/
* Bootstrap (MIT) http://getbootstrap.com
* Background FancyPants by Anton Repponen (Free as beer) http://thepatternlibrary.com/
* VideoJS (MIT) http://www.videojs.com/

###Backend

* NodeJS http://nodejs.org/
* ExpressJS (MIT) http://expressjs.com/
* Uses FFMPEG binaries (GPL). You can get the sources from https://www.ffmpeg.org/download.html