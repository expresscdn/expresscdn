var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var MediaCategory = new Schema({
	name: String,
	description: String,
	mediaFiles: [{type: Schema.ObjectId, ref: 'MediaContent'}]
});

module.exports = mongoose.model('MediaCategory', MediaCategory);