var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Group = new Schema({
	name: String,
	description: String,
	users: [{type: Schema.ObjectId, ref: 'User'}]
});

module.exports = mongoose.model('Group', Group);