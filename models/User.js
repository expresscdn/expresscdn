var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var passportLocalMongoose = require('passport-local-mongoose');

var User = new Schema({
	username: String,
	email: String,
	group: {type: Schema.ObjectId, ref: 'Group'},
	isAdmin: {type: Boolean, default: false},
	isMod: {type: Boolean, default: false},
	mediaFiles: [{type: Schema.ObjectId, ref: 'MediaContent'}]
});

User.plugin(passportLocalMongoose);

module.exports = mongoose.model('User', User);