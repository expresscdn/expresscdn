var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var MediaContent = new Schema({
	name: String,
	description: String,
	accepted: {type: Boolean, default: false},
	user: {type: Schema.ObjectId, ref: 'User'},
	categories: [{type: Schema.ObjectId, ref: 'MediaCategory'}],
	date: {type: Date, default: Date.now},
	mediaFile: String,
	totalViews: {type: Number, default: 0}
});

module.exports = mongoose.model('MediaContent', MediaContent);